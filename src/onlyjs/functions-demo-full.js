function allCommonFunctions() {
    console.log("ALL COMMON FUNCTIONS");
    function commonFunction() {
        console.log("[1] Some stuff from common function");
    }
    commonFunction();

    let functionInVar = function () {
        console.log("[2] Stuff from functionInVar commmon function");
    }
    functionInVar();

    let selfCallFunctionInVar = function () {
        console.log("[3] Stuff from self-call function in variable");
    }();
}

allCommonFunctions();



function allShortArrow(){
    console.log("ALL SHORT ARROW FUNCTIONS");

    let shortArrow = () => { console.log("[1] Hello from short-arrow function"); }
    shortArrow();

    let selfCallShArrow = (() => { console.log("[2] Hello from self-call short arrow func"); })();
}


allShortArrow();