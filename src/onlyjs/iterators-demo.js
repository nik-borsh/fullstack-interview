function iterator(arr) {
    let nextIndex = 0;
    return {
        next: function() {
            return nextIndex < arr.length ?
                {value:arr[nextIndex++], done:false} :
                {done:true};
        }
    }
}


let it = iterator(["a","b","c"]);
console.log(it.next().value);
console.log(it.next().value);
console.log(it.next().value);
console.log(it.next().done);



function customIter(arr) {
    return {
        index: 0,
        allUsedIndexes: [],
        next: function() {
            this.allUsedIndexes.push(this.index);
            return this.index < arr.length ? {
                value: arr[this.index++],
                done: false,
                dump: {
                    indexBeforeNext: this.index-1,
                    indexAfterNext: this.index,
                    allUsedIndexes: this.allUsedIndexes,
                    currentCallContext: this
                },

            } : {
                done: true
            }
        },
    }
}


it = customIter(["a","b","c"]);
console.log(it.next().dump);




