// why Math.min() > Math.max()
// Because Math.min() returns Infinity and Math.max() returns -Infinity
// why they return such strange result? :
// because both functions goes from first arg,
// default for min() is +Infinity because it checks
// 'is left number bigger, then right' e.t.c.
// same thing for max(), but first number is +Infinity and it checks
// 'is left number smaller then right'

console.log(Math.min());
console.log(Math.max());
console.log(Math.min() > Math.max());


