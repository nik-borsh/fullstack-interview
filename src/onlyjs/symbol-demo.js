'use strict'

class User {
    constructor(name,age,sex=undefined) {
        this._name = String(name);
        this._age = String(age);
        this._sex = String(sex);
        this._UUID = Symbol(Math.random()*(100**10));
    }
    get name(){
        return this._name;
    }
    get age(){
        return this._age;
    }
    get sex(){
        return this._sex;
    }
    get UUID(){
        return this._UUID;
    }
    set name(name){
        this._name = String(name);
    }
    set age(age){
        this._age = String(age);
    }
    set sex(sex){
        this._sex = String(sex);
    }
}




// > UUID`s and other id`s usage
let nik = new User("Nikita","16","male");
console.log(`Welcome, ${nik.name} !\n Your UUID: ${nik.UUID.toString()}`);



// Besides of ids usage Symbol can be used for other unique vars:
// vars which should be different also if they are equal:

let n1 = Symbol("asd");
let n2 = Symbol("asd");

console.log(n1 == n2); //false
console.log(n1 === n2); //false
console.log(n1.toString() == n2.toString()); //true

console.log(typeof NaN == typeof NaN);
