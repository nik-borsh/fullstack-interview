let deepCopy = (obj) => { return new Object(JSON.parse(JSON.stringify(obj))); }

let Site = function (domain,port){
    this.domain = String(domain);
    this.port = String(port);
    this.link = String(this.domain + ":" + this.port);
}

let User = new Object();
User.name = "Nikita";
User.surname = "Borshov";


let Admin = function (userObject,siteObject) {
    this.userObjectCopy = null;
    this.siteObjectCopy = null;
}


function demoShallowCopyVSDeepCopy() {
    console.log("---------BEFORE MODIFYING")
    console.log("Nikita:")
    console.log(User);
    let nikitaCopy = deepCopy(User);
    console.log("Nikita copy:\n")
    console.log(nikitaCopy);
    console.log("---------AFTER MODIFYING Nikita object");
    User.name = "Ashot";
    console.log("Nikita:")
    console.log(User);
    console.log("Nikita copy:")
    console.log(nikitaCopy);
}

function deepCopyDemo() {
    let Site = {
        "Host": {
            "Server": {
                "Port": 9080,
                "protocol": "http"
            }
        }
    }
    let site1 = Site;
    let site2 = deepCopy(Site);
    console.log(site1);
    console.log(site2.Host.Server.Port = 9);
    console.log(site2);
}

deepCopyDemo();
