const httpConnection = Object(function(httpAddr){
    console.log(`NEW CONNECTION`);
    this.httpAddr = httpAddr;
    this.UUID = Symbol(Math.floor(Math.random()*10**10));
    this.init = function() { console.log("inited"); };
    this.close = function() { console.log("closed"); };
});


//row below shows that apply and call work with context, not with object:
Object.freeze(httpConnection);
httpConnection.a = 1;
console.log(httpConnection.a); // => undefined if object freezed

let someNotStableUrl = new httpConnection("lol.com");
const hepic2 = new httpConnection("http://de9.sipcapture.io:8002");
const hepic3 = new httpConnection("http://de3.qxip.net:9080/");

function isHepic3(value) {
    this.version = Boolean(value);
}

console.log(hepic2.UUID);

isHepic3.call(hepic3,1)
console.log(hepic3.version);

console.log(someNotStableUrl.UUID);
someNotStableUrl = "kek.com"; //show difference between
console.log(someNotStableUrl);

//bind demo
global.addr = "global:lol";
let wwwAddr = {
    addr : "lol:lol",
    getAddr : function(){ return this.addr }
}


let ad = wwwAddr.getAddr.bind(wwwAddr);

console.log(ad()); //lol:lol
ad = wwwAddr.getAddr;

console.log(ad()); //global:lol
