//base
let someBooleanAfterTernar = (true) ? "result if true" : "result if else";
console.log(someBooleanAfterTernar);

//useful
let User = Object(function(name){
    this.name = String(name);
    this.isAdmin = true;
    this.isUserAdmin = () => this.isAdmin;
});


let Page = Object(function(){ this.link = "local:local"});
let Nik = new User("Nik");
Nik.isAdmin = true;

let deletePage = function(){ return Nik.isAdmin ?  "DELETE" : "NO ADMIN ACCESS"; };

console.log(deletePage());
Nik.isAdmin = false;
console.log(deletePage());


//ternar in ternar algorithm

let obj = {
    sides: 3,
    corners: [1,100,20]
}
let isTriangle = (obj) => { return obj.sides == 3 ? ( obj.corners.length == 3): false};
console.log(isTriangle(obj));