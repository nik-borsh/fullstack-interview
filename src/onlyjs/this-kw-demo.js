function returnThis(){
    return this
}

function thisInsideShortArrow(){
    return () => { this };
}

function thisInsideFunction(){
    return function(){
        return this
    }
}

let thisInVariable = function() {
    return this
}

let thisInShortArrowVar = () => { this };


let your_scope = window;
console.log(returnThis() === your_scope);           //test1 in global, so result -> true
console.log(thisInsideShortArrow() === your_scope); //test2 returns short-arrow func in local -> false
console.log(thisInsideFunction() === your_scope);   //test3 returns has other function inside -> false
console.log(thisInVariable() === your_scope);       //test4 returns has other function inside -> true
console.log(thisInShortArrowVar() === your_scope);  //test5 this in var that in global scope -> false
