// функция генератор возвращает итерируемый обьект

function* randomGenerator(){
    let randomNum = function() {
        return {
            "randomNum": Math.floor(Math.random()*100)
        }
    }
    for(let num=0; num<5; num++) {
        yield randomNum();
    }
}

let generatedRandomNumbers = randomGenerator();

for(let randomNumber of generatedRandomNumbers){
    console.log(randomNumber);
}


//generate array with generators
let arrWithRandomNumbers = [...(function* (){ for(let i=0; i<5; i++){ yield Math.floor(Math.random()*100);}})()];
console.log(arrWithRandomNumbers);



