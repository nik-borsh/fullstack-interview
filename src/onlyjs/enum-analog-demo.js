const serverAPI = {
    NAME: "API:123.123.123",
    PROTOCOL: "http",
    WEBSERVER: {
        TYPE: "APACHE",
        VERSION: "5.6.7",
        SUBDOMAIN: ".com"
    },
    EXEC: {
        connect: function() {
            console.log("Connecting, wait....");
            setTimeout(() => { console.log("Connected")},Math.floor(Math.random()*1000));
        },
        closeConnection: function () {
            console.log("Connection closed");
        }
    }
}

serverAPI.EXEC.connect();
// serverAPI.EXEC.closeConnection();