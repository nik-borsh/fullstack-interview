const REQUIRED_COLORS = {
    RED: "red",
    GREEN: "green",
    BLACK: "black",
    WHITE: "white",
}

const REQUIRED_API = {
    context: document.getElementById("canvas").getContext('2d'),
    pixel: {
        size: 10
    },
    direction: {
        onY: {
            UP: 10,
            DOWN: -10,
        },
        onX: {
            RIGHT: 10,
            LEFT: -10,
        },
        none: 0
    },
    LOOP: 20, //milli seconds
}