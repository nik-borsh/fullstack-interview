const gameFieldObj = function() {
    actions.drawerAPIcanvas = REQUIRED_API.context;
    this.drawerAPIcanvas = actions.drawerAPIcanvas;
    this.startsFrom = 0;
    this.endsWith = 300;
    this.render = function () {
        for (let coordOfSquare = 0; coordOfSquare <= this.endsWith; coordOfSquare++) {
            actions.drawPixel(coordOfSquare, this.startsFrom, REQUIRED_COLORS.BLACK);
            actions.drawPixel(coordOfSquare, this.endsWith, REQUIRED_COLORS.BLACK);
            actions.drawPixel(this.startsFrom, coordOfSquare, REQUIRED_COLORS.BLACK);
            actions.drawPixel(this.endsWith, coordOfSquare, REQUIRED_COLORS.BLACK);
        };

    }
}


const initGameField = function(){
    const gameField = new gameFieldObj();
    gameField.render();
    console.log("START GAME -> init field for snake");
}