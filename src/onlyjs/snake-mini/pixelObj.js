let Pixel = function(x,y,style){
    coords: {
        this.x = Number(x);
        this.y = Number(y);
    }
    this.style = String(style);
}


let actions = {
    drawPixel: function (x, y, style) {
        actions.drawerAPIcanvas = REQUIRED_API.context;
        actions.drawerAPIcanvas.fillStyle = (String(style));
        actions.drawerAPIcanvas.fillRect(x, y, 10, 10);
        actions.drawerAPIcanvas.stroke();
    },

    move: function (fromXY, toXY) {
        let copyOfFillStyle = actions.drawerAPIcanvas.fillStyle;
        actions.drawPixel(fromXY[0], fromXY[1], "white");
        actions.drawPixel(toXY[0], toXY[1], copyOfFillStyle);
    },
}