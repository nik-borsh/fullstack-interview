
let snake = function(){
    this.currentDirection = REQUIRED_API.direction.none;
    this.body = Array(0);
    this.head = {coords:{x: 100,y:100}};
    this.snakeRender = function(){
        actions.drawPixel(this.head.x,this.head.y,REQUIRED_COLORS.GREEN);
    }
}


let x = 60;
let y = 220;
function changeSnakeDirection(direction) { // here
    let ranX = x;
    let ranY = y-REQUIRED_API.pixel.size;
    if (ranX >= 50 && ranY >= 50 && ranX <=250 && ranY <=250) {
        actions.drawPixel(ranX,ranY,REQUIRED_COLORS.GREEN);
        actions.drawPixel(x,y,REQUIRED_COLORS.WHITE);
        x = ranX;
        y = ranY;
    } else { drawRandomPixel() }
}


let initSnakeWidget = function (){
    let id= setInterval(() => { changeSnakeDirection("UP") },REQUIRED_API.LOOP);
}